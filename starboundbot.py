import datetime
import sqlite3

import praw
import feedparser
from config import Config


class StarboundBot:
    def __init__(self):
        self.reddit = praw.Reddit(Config.USER_AGENT)
        self.reddit.set_oauth_app_info(Config.OAUTH_ID, Config.OAUTH_SECRET, Config.REDIRECT_URI)

    @staticmethod
    def get_previous_rss(conn):
        cur = conn.cursor()
        feed = feedparser.parse(Config.RSS)
        for entry in feed.entries:
            cur.execute('INSERT INTO entries(thing_id, title, link, published) VALUES(?,?,?,?)',
                        (None, entry.title, entry.link, entry.published))

        conn.commit()

    def init_db(self):
        conn = sqlite3.connect(Config.RSS_DB)
        conn.executescript(open('rss.sql').read())
        self.get_previous_rss(conn=conn)

    def manage_rss(self):
        conn = sqlite3.connect(Config.RSS_DB, detect_types=sqlite3.PARSE_DECLTYPES)
        conn.row_factory = sqlite3.Row
        cur = conn.cursor()
        cur.execute('SELECT * FROM entries')

        l = [s['title'] for s in cur]

        feed = feedparser.parse(Config.RSS)
        for entry in feed.entries:
            if entry.title not in l:
                try:
                    self.reddit.refresh_access_information(Config.REFRESH_TOKEN)
                    s = self.reddit.submit(subreddit=Config.ACTIVE_SUB, title=entry.title, url=entry.link)
                    s.add_comment(
                        '`Informative. This post was made automatically. If there are any questions/comments, please '
                        'message` ' + Config.ADMIN + '.')
                    cur.execute('INSERT INTO entries(thing_id, title, link, published) VALUES (?,?,?,?)',
                                (s.id, entry.title, entry.link, entry.published))
                    print("Post Created in {} :: {}\n".format(Config.ACTIVE_SUB, s.url))
                except praw.errors.AlreadySubmitted:
                    cur.execute('INSERT INTO entries(thing_id, title, link, published) VALUES (?,?,?,?)',
                                ("0", entry.title, entry.link, entry.published))
                    print("Post Already Submitted in {} :: {}\n".format(Config.ACTIVE_SUB, entry.link))

        conn.commit()

    def main(self):
        running = True

        while running:
            try:
                self.manage_rss()
            except KeyboardInterrupt:
                running = False
            except Exception as e:
                now = datetime.datetime.now()
                print(now.strftime("%m-%d-%Y %H:%M") + ' Error: ' + str(e) + '\n')
                continue



if __name__ == '__main__':
    bot = StarboundBot()
    bot.main()