# StarboundBot (beta) v1.0.0

## About

This is a bot for posting development and playtesting streams to the Starbound subreddit. Its feature set will expand as I find more uses for it, especially post release.

Please show some support to the guys at the [Official Starbound blog](http://www.playstarbound.com/)!

## Features

* Can post to any single subreddit.

* Will post links to posts from the official Starbound blog.

##Dependencies

* praw

* requests

* feedparser

## Questions/Comments

* Please message /u/Mr_Dionysus on reddit
