class Config:
    USER_AGENT = 'StarboundBot by /u/Mr_Dionysus'
    ADMIN = '/u/Mr_Dionysus'

    ACTIVE_SUB = 'StarboundBot'
    RSS = 'http://playstarbound.com/feed/'
    RSS_DB = 'rss.db'

    OAUTH_ID = '*****'
    OAUTH_SECRET = '*****'
    REDIRECT_URI = 'http://127.0.0.1:65010/authorize_callback'

    SCOPES = ['edit', 'identity', 'modconfig', 'modflair', 'modlog', 'modposts', 'mysubreddits', 'privatemessages',
              'read', 'submit', 'subscribe', 'vote']

    REFRESH_TOKEN = '*****'